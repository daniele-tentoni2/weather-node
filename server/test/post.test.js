/**
 * Post Test Script.
 *
 * @summary Test post script api endpoint on mongodb schema.
 * @author Daniele Tentoni
 * @since 0.0.3
 */

const Post = require("../db/post");

const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../index");

chai.should();
chai.use(chaiHttp);

// Parent Block.
describe("Posts", () => {
  beforeEach(async () => await Post.deleteMany({}));

  /*
   * Test the /GET route.
   */
  describe("/GET posts", () => {
    it("it should GET all the posts", async () => {
      const result = await chai.request(server)
        .get("/api/posts")
        .set("content-type", "application/json");
      result.should.have.status(200);
      result.body.should.be.a("array");
      result.body.length.should.be.eql(0);
    });
  });

  describe("/POST post", () => {
    it("should not POST a post without title field", async () => {
      const post = {};
      const result = await chai.request(server)
        .post("/api/post")
        .set("connection", "keep alive")
        .set("content-type", "application/json")
        .send(post);
      result.should.have.status(500);
      result.body.should.be.a("object");
      result.body.should.have.property("description");
      result.body.should.have.property("statusCode");
    });
    it("should POST a post", async () => {
      const title = "Test post";
      const post = { title };
      const result = await chai.request(server)
        .post("/api/post")
        .set("connection", "keep alive")
        .set("content-type", "application/json")
        .send(post);
      result.should.have.status(200);
      result.body.should.be.a("object");
      result.body.should.have.property("title").eql(title);
      result.body.should.have.property("publishedAt");
    });
  });

  describe("/GET/:id post", () => {
    it("it should GET a post by the given id", async () => {
      const post = new Post({ title: "The Get Post" });
      post.save(async (err, post) => {
        const result = await chai.request(server)
          .get(`/api/post/${post.id}`)
          .set("content-type", "application/json")
          .send(post);
        result.should.have.status(200);
        result.body.should.be.a("object");
        result.body.should.have.property("title").eql(post.title);
        result.body.should.have.property("_id").eql(post.id);
      });
    });
  });

  describe("/PUT/:id post", () => {
    it("should UPDATE a post given the id", async () => {
      const title = "The Put Post"
      const post = new Post({ title })
      const secondTitle = "The Second Put Post"
      post.save(async (err, post) => {
        const result = await chai.request(server)
          .put(`/api/post/${post.id}`)
          .set("content-type", "application/json")
          .send({ title: secondTitle });
        result.should.have.status(200);
        result.body.should.be.a("object");
        result.body.should.have.property("title").eql(secondTitle);
      })
    })
  })

  describe("/DELETE/:id post", () => {
    it("should DELETE a post given the id", async () => {
      const title = "The Put Post"
      const post = new Post({ title })
      post.save(async (err, post) => {
        const result = await chai.request(server)
          .set("content-type", "application/json")
          .delete(`/api/post/${post.id}`);
        result.should.have.status(200)
        result.body.should.be.a("object")
        result.body.should.have.property("title").eql(post.title)
        result.body.should.have.property("_id").eql(post.id)
      })
    })
  })
})
