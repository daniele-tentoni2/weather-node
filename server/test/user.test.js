/**
 * User's unit test.
 *
 * @author Daniele Tentoni
 * @since 0.0.3
 */

const User = require("../db/user")

const chai = require("chai")
const chaiHttp = require("chai-http")
const server = require("../index")

chai.should()
chai.use(chaiHttp)

describe("Users", () => {
  beforeEach(async () => await User.deleteMany({}));

  /*
   * Test the /GET route.
   */
  describe("/GET users", () => {
    it("it should GET all users", async () => {
      const result = await chai.request(server)
        .get("/api/users")
        .set("content-type", "application/json");
      result.should.have.status(200);
      result.body.should.be.a("array");
      result.body.length.should.be.eql(0);
    });
  });
});