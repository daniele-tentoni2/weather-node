/**
 * OpenWeather Forecast Controller.
 *
 * Execute calls to OpenWeather apis in order to get forecasts.
 *
 * @author Daniele Tentoni
 * @since 0.0.4
 */

const axios = require("axios");

const require_forecast = async (time, city, country) => {
  if (
    typeof process.env.OPENWEATHER_URL === "undefined" ||
    typeof process.env.OPENWEATHER_KEY === "undefined"
  ) {
    return { description: "You need to set environment variables" };
  }

  try {
    const response = await axios_request(time, city, country);
    console.log("Axios success: %s", response);
    return response.data;
  } catch (error) {
    console.error("Axios error: %s", error);
    return error;
  }
};

const axios_request = async (time, city, country) => {
  const openweather_url = parseTimeToUrl(process.env.OPENWEATHER_URL, time);
  try {
    const body = await axios.get(openweather_url, {
      params: { city, country, key: process.env.OPENWEATHER_KEY },
    });
    console.log("Response received %s(%s)", body.statusText, body.status);
    return body.data;
  } catch (error) {
    if (error.response) {
      const message =
        "Response error: " +
        error.response.statusText +
        "(" +
        error.response.status +
        ") to request: " +
        time;
      console.error(message);
      console.error(
        "Headers: %o => %s",
        error.response.headers,
        error.response.data
      );
      return message;
    } else if (error.request) {
      const message = "Error without response: " + error.request;
      console.error(message);
      return message;
    } else {
      const message = "Error setting up the request: " + error.message;
      console.error(message);
      return message;
    }
  }
};

/**
 * Parse arguments to a well formed url to call for weather forecasts.
 * @param {string} url base url of weather service.
 * @param {string} time time to forecast.
 * @returns composed url.
 */
const parseTimeToUrl = (url, time) => {
  if (typeof time === "string" && time === "current") {
    return url + "/weather";
  }
  return url;
};

module.exports = { require_forecast };
