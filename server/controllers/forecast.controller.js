/**
 * Aggregate Forecast model.
 *
 * Persist many forecasts to show them and provide data analisys.
 * Perform request to weather services supported an join results.
 * In 0.0.4, the service auth_key and url validation is moved to single controller files.
 * @todo Add validation on update: https://mongoosejs.com/docs/validation.html#update-validators
 *
 * @author Daniele Tentoni
 * @since 0.0.4
 */

// const forecast = require("../db/forecast");
// const aggregate_forecast = require("../db/aggregate_forecast");
const weatherbit = require("./weatherbit.controller");
// const openweather = require("./openweather.controller");
const { check_country } = require("./regex.utils");

/**
 * Require a forecast to Weatherbit.
 * @param {String} time time string to require.
 * @param {String} city City to forecast.
 * @param {String} country Country to forecase. Use 2 letter code.
 * @returns Forecast Promise.
 */
const require_forecast = async (req, res, next) => {
  // Check params
  if (typeof req.params.time !== "string") {
    return next({
      description: "You need to require a time window of forecasts.",
    });
  }

  if (typeof req.query.city !== "string") {
    return next({
      description: "You need to require a city for forecasts.",
    });
  }

  if (
    typeof req.query.country !== "string" ||
    !check_country(req.query.country)
  ) {
    return next({
      description: "You need to require a country for forecasts.",
    });
  }

  const time = req.params.time;
  const city = req.query.city;
  const country = req.query.country;

  try {
    // TODO: Add request count.
    // TODO: Add request check.
    // TODO: Don't await single calls. Use let [someResult, anotherResult] = await Promise.all([someCall(), anotherCall()]);
    const bit_result = await weatherbit.require_forecast(time, city, country);
    // TODO: Test openweather controller.
    // const open_result = await openweather.require_forecast(time, city, country);
    // Format data.
    return res.status(200).json(bit_result);
  } catch (error) {
    console.error("Forecast error: %s", error);
    // Error propagation.
    return next(error);
  }
};

module.exports = { require_forecast };
