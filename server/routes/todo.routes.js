const express = require("express")
const TodoRoute = express.Router()

const Todo = require("../db/todo")

/*
 * GET all todo. Optionally, fetch by completed.
 */
TodoRoute.route("/todo").get((req, res, next) => {
  // First, find all todo.
  let all = Todo.find()
  // Fetch for completed query param and apply it.
  const completed = req.query.completed
  if (typeof completed !== "undefined") {
    all = all.byCompleted(completed)
  }
  // Finally execute the function.
  all.exec((error, data) => {
    if (error) {
      return next(error)
    }
    return res.status(200).json(data)
  })
})

/*
 * GET todo by id. Param value.
 */
TodoRoute.route("/todo/:id").get((req, res, next) => {
  Todo.findById(req.params.id, (error, data) => {
    if (error) {
      return next(error)
    }
    return res.status(200).json(data)
  })
})

/*
 * POST todo. Completed field is set to false by default.
 */
TodoRoute.route("/todo").post((req, res) => {
  const newTodo = new Todo({
    title: req.body.todo,
    completed: false
  })
  newTodo.save((err) => {
    if (err) {
      res.send(`Error ${err} while create ${newTodo}.`)
    }
    return res.status(200).send("Todo added")
  })
})

/*
 * PUT todo. Can update all fields.
 */
TodoRoute.route("/todo/:id").put((req, res) => {
  Todo.findByIdAndUpdate(req.params.id, { completed: true }, (err, todo) => {
    if (!err) {
      res.send("Good work")
    }
    return res.status(200).send(todo);
  })
})

/*
 * DELETE todo. Can delete all todos.
 */
TodoRoute.route("/todo/:id").delete((req, res) => {
  const query = { _id: req.params.id }
  Todo.deleteOne(query, (err) => {
    if (err) {
      res.send("Error while deleting todo")
    }
    return res.status(200).send("Todo deleted")
  })
})

module.exports = TodoRoute
