/**
 * Aggregate Forecast model.
 *
 * Persist many forecasts to show them and provide data analisys.
 * @todo Add validation on update: https://mongoosejs.com/docs/validation.html#update-validators
 * 
 * @author Daniele Tentoni
 * @since 0.0.4
 */

const mongoose = require("mongoose");

const aggregation = new mongoose.Schema({
  // City of the forecast.
  city: {
    type: String,
    required: [true, "You need to set a city for a forecast"]
  },
  // Country. Must be a 2-letter string.
  country: {
    type: String,
    required: [true, "You need to set a country for your forecase"],
    validate: {
      validator: function (v) {
        return /^[A-Z]{2}$/.test(v);
      },
      message: props => `${props.value} is not a postal code!`
    },
  },
  // Postal code. Must match ^([0-9]{5})([\-]{1}[0-9]{4})?$
  postal_code: {
    type: Number,
    validate: {
      validator: function (v) {
        return /^\d{5}$/.test(v);
      },
      message: props => `${props.value} is not a postal code!`
    },
  },
  weatherbit: {
    type: String,
    required: () => typeof this.openweather !== "undefined"
  },
  openweather: {
    type: String,
    required: () => typeof this.weatherbit !== "undefined"
  }
}, {
  collection: "aggregate_forecasts"
});

module.exports = mongoose.model("aggregation_forecast", aggregation);