/**
 * Application Main Module.
 *
 * In this file, console.error must be allowed to log on server console some
 * errors that can may occur. To think at a log service to make this not
 * happen.
 *
 * @author Daniele Tentoni
 * @since 0.0.1
 */

/* eslint no-console: ["error", { allow: [ "error" ] }] */

const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");

/*
 * Application configuration.
 */

const port = process.env.PORT || 8080; // Get default port.
const app = express(); // Init express app.

app.use(cors());
app.use(express.json()); // Parse application/json
app.use(express.urlencoded({ extended: false })); // Parse application/x-www-form-urlencoded

/*
 * Database connection.
 */

// Wait for the promise to don't block main thread.
mongoose.Promise = global.Promise;
// Optional. Use this if you create a lot of connections and don't want
// to copy/paste `{ useNewUrlParser: true }`.
mongoose.set("useNewUrlParser", true);
// Make Mongoose use `findOneAndUpdate()`. Note that this option is `true`
// by default, you need to set it to false.
mongoose.set("useFindAndModify", false);
mongoose
  .connect(process.env.MONGODB_URI, {
    useUnifiedTopology: true,
  })
  .then(() => {})
  .catch((error) => {
    console.error(" ===== ");
    console.error("Database couldn't be connected to: " + error);
    console.error(" ===== ");
  });

// Our function which adds two numbers and returns the result.
const addNumbers = (firstNumber, secondNumber) => {
  // Check that input is a number.
  if (
    typeof Number(firstNumber) !== "number" ||
    typeof Number(secondNumber) !== "number"
  ) {
    return "Values should be integer or numbers";
  }
  return Number(firstNumber) + Number(secondNumber);
};

// Add points endpoint.
app.post("/api/add", (req, res) => {
  const { firstNumber, secondNumber } = req.body;
  const result = addNumbers(firstNumber, secondNumber);
  return res.status(200).send({
    result,
  });
});

// Require CRUD post apis.
const postAPI = require("./routes/post.routes");
app.use("/api", postAPI);

// Require CRUD todo apis.
const todoAPI = require("./routes/todo.routes");
app.use("/api", todoAPI);

// Require CRUD users apis.
const usersApi = require("./routes/user.routes");
app.use("/api", usersApi);

// Require CRUD forecasts apis.
const forecastApi = require("./routes/forecast.routes");
app.use("/api", forecastApi);

// App entry point.
app.get("/", (req, res) =>
  res.status(200).json({
    message: "Welcome to the Weather app.",
  })
);

// TODO: Remove this endpoint in production environment.
app.get("/env", (req, res) => {
  res.status(200).json(process.env);
});

// Setup a catch-all point.
app.get("*", (req, res) =>
  res.status(200).json({
    message: "Here there's nothing. 404?",
  })
);

const { sendMessageTo } = require("./telegram");

// eslint-disable-next-line no-unused-vars
app.use(function (err, req, res, next) {
  console.error("Called last error handler: %@", err);

  if (!err.statusCode) {
    err.statusCode = 500;
  }

  return res.status(err.statusCode).json(err);
});

const server = app.listen(port, (err) => {
  const serverHost = server.address().address;
  const serverPort = server.address().port;
  const serverFamily = server.address().family;
  if (!err) {
    console.error(`App started on ${process.env.URL}:${port}`);
    console.error(
      `Server address is ${serverFamily}_${serverHost}:${serverPort}`
    );
    if (process.env.TELEGRAM_CHAT_NOTIFICATION) {
      sendMessageTo(
        process.env.TELEGRAM_CHAT_NOTIFICATION,
        `Application deployed and running without errors on ${process.env.URL}.`
      );
    }
  } else {
    console.error("App started with errors: ", err);
  }
});

module.exports = app;
